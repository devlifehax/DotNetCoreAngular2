import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UniversalModule } from 'angular2-universal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { HttpModule } from '@angular/http'

import { AppComponent } from './components/app.component'
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { FetchDataComponent } from './components/fetchdata/fetchdata.component';
import { CounterComponent } from './components/counter/counter.component';

import { CoursesComponent } from './components/courses.component';
import { AuthorsComponent } from './components/authors.component';
import { MessagesComponent } from './components/messages.component';
import { FavoriteComponent } from './components/favorite.component';
import { LikeComponent } from './components/like.component';
import { VoteComponent } from './components/vote.component';
import { TweetComponent } from './components/tweet.component';
import { ZippyComponent } from './components/zippy.component';
import { SubscriptionFormComponent } from './components/subscription-form.component';
import { SignUpFormComponent } from './components/signup-form.component';
import { PasswordResetComponent } from './components/password-reset.component';
import { ObservablesComponent } from './components/observables.component';
import { PostComponent } from './components/post.component';

import { CourseService } from './components/course.service';
import { AuthorService } from './components/author.service';
import { MessagesService } from './components/messages.service';
import { TweetService } from './components/tweet.service';
import { PostService } from './components/post.service';

import { AutoGrowDirective } from './components/auto-grow.directive';

@NgModule({
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent,
        NavMenuComponent,
        CounterComponent,
        FetchDataComponent,
        HomeComponent,
        CoursesComponent,
        AuthorsComponent,
        LikeComponent,
        VoteComponent,
        MessagesComponent,
        AutoGrowDirective,
        FavoriteComponent,
        TweetComponent,
        ZippyComponent,
        SubscriptionFormComponent,
        SignUpFormComponent,
        PasswordResetComponent,
        ObservablesComponent,
        PostComponent
    ],
    imports: [
        UniversalModule, // Must be first import. This automatically imports BrowserModule, HttpModule, and JsonpModule too.
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'counter', component: CounterComponent },
            { path: 'fetch-data', component: FetchDataComponent },
            { path: 'password-reset', component: PasswordResetComponent },
            { path: 'observables', component: ObservablesComponent },
            { path: 'posts', component: PostComponent},
            { path: '**', redirectTo: 'home' }
        ]),
        FormsModule,
        ReactiveFormsModule,
    ],
    providers: [CourseService, AuthorService, MessagesService, TweetService, PostService],

})
export class AppModule {
}
