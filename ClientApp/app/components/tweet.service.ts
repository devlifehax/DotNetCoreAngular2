import { Injectable } from '@angular/core';

Injectable()
export class TweetService {
    tweets = [
        {   
            id : 0,
            name : "HM",
            handle : "@devlifehax",
            description : "I live, breathe, and eat, programming!",
            likes : 11,
            liked: true
        },
        {   
            id : 1,
            name : "Steph",
            handle : "@boobah18",
            description : "You rock!",
            likes : 3,
            liked : false
        },
        {
            id : 2,
            name : "SbQ",
            handle : "@HoovaQ",
            description : "YURRRRRDA MEEEEAN",
            likes : 7,
            liked : true
        }
        ]

    getTweets() {
        return this.tweets;
    }
}