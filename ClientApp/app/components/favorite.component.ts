import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'favorite',
    template: `
    <i
        (click)="onClick()"
        class="glyphicon"
        [class.glyphicon-star]="isFavorite"
        [class.glyphicon-star-empty]="!isFavorite">
    </i>`
})


export class FavoriteComponent {
    @Input() isFavorite = false;
    @Output() change = new EventEmitter();

    onClick() {
        this.isFavorite = !this.isFavorite;
        this.change.emit({ newValue: this.isFavorite });
    }
}