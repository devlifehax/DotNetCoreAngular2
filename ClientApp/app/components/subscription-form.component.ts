import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';


@Component({
    selector: 'subscription-form',
    templateUrl: './subscription-form.component.html',
    styleUrls: ['./styles.css']
})

export class SubscriptionFormComponent {
    frequencies = [
        { id: 1, label: 'Daily' }, 
        { id: 2, label: 'Weekly' },
        { id: 3, label: 'Monthly' }
    ];

    onSubmit(form) {
        console.log(form.value);
    }
}