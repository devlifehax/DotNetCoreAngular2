import { FormControl, FormGroup, AbstractControl } from '@angular/forms';

export class PasswordValidator {
    static passwordsDoNotMatch(control: FormGroup) {
        var newPassword = control.get("newPassword").value;
        var confirmPassword = control.get("confirmNewPassword").value;

        if (newPassword == "" || confirmPassword == "") {
            return null;
        }

        return newPassword === confirmPassword ? null : { passwordsDoNotMatch: true }
    }
}