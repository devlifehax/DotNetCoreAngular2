import {Component, Input} from '@angular/core';
import {LikeComponent} from './like.component';

@Component({
    selector: 'tweet',
    template: `
    <div class="media">
        <div class="media-left">
            <img class="media-object" src="http://lorempixel.com/100/100/people?{{id}}" alt="Generic placeholder image">
        </div>
        <div class="media-body">
            <h4 class="media-heading">{{ name }} <span class="twitter-handle">{{ handle }}</span></h4>
            {{ description }}
            <br>
            <like [likes]="likes" [isLiked]="liked"></like>
        </div>
    </div>
    `,
    styles: [`
    .twitter-handle {
        color: #ccc;    
    }
    `]
})

export class TweetComponent {
    @Input() id = 0;
    @Input() name = "default";
    @Input() handle = "@default";
    @Input() description = "lorem ipsum";
    @Input() likes = -1;
    @Input() liked;
}