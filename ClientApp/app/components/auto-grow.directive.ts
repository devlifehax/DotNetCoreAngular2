import { Directive, ElementRef, Renderer } from '@angular/core';

@Directive({
    selector: '[autoGrow]',
    host: {
        '(focus)': 'onFocus()',
        '(blur)': 'onBlur()'
    }
})

export class AutoGrowDirective {
    constructor(private el: ElementRef, private render: Renderer){
    }

    onFocus(){
        console.log("onFocus called");
        this.render.setElementStyle(this.el.nativeElement, 'width', '200px');
    }

    onBlur(){
        console.log("onBlur called");
        this.render.setElementStyle(this.el.nativeElement, 'width', '120px');
    }
}