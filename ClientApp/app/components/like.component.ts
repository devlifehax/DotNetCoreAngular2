import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'like',
    template: `
    <i class="glyphicon glyphicon-heart" 
    (click)="onClick()"
    [style.color]="isLiked ? 'deeppink': '#ccc'"></i>
    <span>{{likes}}</span>
    `,
    styles: [`
    .glyphicon-heart {
        cursor: pointer;
    }
    `]
})

export class LikeComponent{
    @Input() likes = 10;
    @Input() isLiked = false;

    onClick() {
        this.isLiked = !this.isLiked;
        this.isLiked ? this.likes++: this.likes--;
    }
}