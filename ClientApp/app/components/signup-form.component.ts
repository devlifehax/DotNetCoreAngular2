import {Component} from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators, FormBuilder } from '@angular/forms';
import { UserNameValidator } from './userNameValidator';

@Component({
    selector: 'signup-form',
    templateUrl: './signup-form.component.html',
    styleUrls: ['./styles.css']
})

export class SignUpFormComponent {
    form : FormGroup;

    constructor (fb : FormBuilder) {
        this.form = fb.group({
            username: ['', Validators.compose([
                Validators.required, 
                UserNameValidator.cannotContainSpace
            ]), UserNameValidator.isUniqueUsername],
            password: ['', Validators.required]
        })
    }

    // Long form way of creating a FormGroup and defining its FormControls and their validators.
    // form = new FormGroup({
    //     username: new FormControl('', Validators.required),
    //     password: new FormControl('', Validators.required)
    // })

    signUp() {
        this.form.get('username').setErrors({
            invalidLogin: true
        });

        console.log(this.form.value);
        console.log(this.form);
    }

    log(x) {
        console.log(x);
    }
}