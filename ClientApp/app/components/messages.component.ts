import { Component } from '@angular/core';
import { MessagesService } from './messages.service';

@Component({
    selector: 'messages',
    template: `<h1>Messages</h1>
    {{ title }}
    <ul>
        <li *ngFor="let message of messages">{{ message }} </li>
    </ul>`,
})

export class MessagesComponent {
    title = "Title for messages"
    messages;

    constructor(private messagesService: MessagesService){
    this.messages = messagesService.getMessages();
    }
}