import {Component} from '@angular/core';
import {CoursesComponent} from './courses.component';
import {AuthorsComponent} from './authors.component';
import {FavoriteComponent} from './favorite.component';
import {LikeComponent} from './like.component';
import {VoteComponent} from './vote.component';
import {TweetComponent} from './tweet.component';
import {TweetService} from './tweet.service';
import {ZippyComponent} from './zippy.component';
import {SubscriptionFormComponent} from './subscription-form.component';

@Component({
    selector: 'app',
    templateUrl: './app.component.html'
})

export class AppComponent { 
    show = false;
    tweets;

    constructor(tweetService: TweetService) {
        this.tweets = tweetService.getTweets();
    }

    post = {
        title: "Title",
        isFavorite: true,
        votes: 200,
        vote: -1
    }

    onFavoriteChange($event) {
        console.log($event);
    }

    onVoteChange($event) {
        console.log($event);
    }
}