import { FormControl } from '@angular/forms';

export class UserNameValidator {
    
    static isUniqueUsername(formControl: FormControl) {
        return new Promise((resolve, reject) => {
            setTimeout(function() {

                if (formControl.value == "hmarquez") {
                    resolve({ isUniqueUsername: false });
                    console.log({ isUniqueUsername: false })
                } else if (formControl.value == "devlifehax") {
                    resolve({ isUniqueUsername: false });
                    console.log({ isUniqueUsername: false })

                } else {
                    resolve(null);
                    console.log({ isUniqueUsername: null })

                }
            }, 2000);
        })
    }

    static cannotContainSpace(formControl : FormControl) {
        if (formControl.value.indexOf(' ') >= 0)
            return { cannotContainSpace : true }

        return null;
    }
}