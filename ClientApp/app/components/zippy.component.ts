import {Component, Input} from '@angular/core';

@Component({
    selector: 'zippy',
    template: `
    <div class="panel panel-default">
        <div class="panel-heading zippy-title"
            (click)="toggle()">
            {{ title }}
            <i 
                class=" pull-right glyphicon"
                [ngClass]="
                {
                    'glyphicon-chevron-up' : show,
                    'glyphicon-chevron-down' : !show
                }">
            </i>
        </div>
        <div *ngIf="show" class="panel-body">
            <ng-content></ng-content>
        </div>
    </div>
    `,
    styles: [`
    .panel {
        width: 300px;
    }

    .zippy-title {
        cursor: hover;
    }

    .zippy-title:hover {
        background: #DEDEDE;
    }
    `]
})

export class ZippyComponent {
    show = true;
    @Input() title: string;

    toggle() {
        this.show = !this.show;
    }
}