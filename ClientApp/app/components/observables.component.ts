﻿import { Component } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validator } from '@angular/forms';
import { Observable } from 'rxjs/Rx';

@Component({
    selector: 'observables',
    templateUrl: './observables.component.html'
})

export class ObservablesComponent {
    form: FormGroup;

    constructor(fb: FormBuilder) {
        this.form = fb.group({
            searchBar: []
        })

        var searchBar = this.form.get("searchBar");
        searchBar.valueChanges
            .debounceTime(400)
            .map(str => (<string>str).split(" ").join("-"))
            .subscribe(x => console.log(x));

        //var observables = Observable.interval(15000);
        //observables
        //    .flatMap(x => {
        //        console.log("Retrieving news from server...");
        //        return Observable.of(["Headline 1", "Headline 2", "Headline 3"]);
        //    })
        //    .subscribe(news => {
        //        console.log(news);
        //    });

        var userStream = Observable.of({
            userId: 0, username: 'devlifehax'
        }).delay(15000);

        var tweetStream = Observable.of(['Going to IND', 'Going to PHL']).delay(5000);

        Observable
            .forkJoin(userStream, tweetStream)
            .map(joined =>
                new Object({user: joined[0], tweets: joined[1]}))
            .subscribe(result => console.log(result));

        //Observable.of([1, 2, 3, 4, 5])
        //    .take(3)
        //    .subscribe(x => console.log(x));

        Observable.from([1, 2, 3])
            .flatMap(x => Observable.of(['a', 'b', 'c']))
            .subscribe(x => console.log(x));



    }
}