﻿import { Component, OnInit } from '@angular/core'
import { PostService } from './post.service';
import { Post } from './post';

@Component({
    selector: 'post',
    templateUrl: './post.component.html'

})
export class PostComponent implements OnInit {
    isLoading = true;
    private posts: Post[];

    ngOnInit(): void {
        this._postService.getPosts()
            .delay(1000)
            .subscribe(posts => {
                this.isLoading = false;
                console.log(posts)
                this.posts = posts;
            });

        
    }

    constructor(private _postService: PostService) {
    }



}