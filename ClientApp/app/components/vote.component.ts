import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'vote',
    template: `
    <div>
        <i 
            class="glyphicon glyphicon-menu-up vote-button" 
            (click)="onUpVote()" 
            [style.color]="myVote > 0 ? 'orange' : 'grey'" >
        </i>

        <span>{{totalVotes + myVote}}</span>

        <i 
            class="glyphicon glyphicon-menu-down vote-button" 
            (click)="onDownVote()" 
            [style.color]="myVote < 0 ? 'orange' : 'grey'">
        </i>
    </div>`,
    styles: [`
    div {
        width: 20px;
        text-align: center;
    }
    .vote-button {
        cursor: pointer;
    }
    `]
})

export class VoteComponent {
    @Input() totalVotes = 0;
    @Input() myVote = 0;
    @Output() change = new EventEmitter();

    onUpVote() {
        this.myVote < 1 ? this.myVote++: this.myVote--;
        this.onVoteChange();
    }

    onDownVote() {
        this.myVote > -1 ? this.myVote--: this.myVote++;
        this.onVoteChange();
    }

    onVoteChange() {
        console.log(this.myVote);
        this.change.emit({ vote : this.myVote });
    
    }

}