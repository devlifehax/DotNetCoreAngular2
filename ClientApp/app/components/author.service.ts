import { Injectable } from '@angular/core';

@Injectable()
export class AuthorService{
    getAuthors() : string[] {
        return ["Hector", "Dean", "Michael"];
    }
}