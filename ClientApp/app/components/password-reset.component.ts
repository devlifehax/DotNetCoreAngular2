import {Component} from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { PasswordValidator } from './passwordValidator';

@Component({
    selector: 'password-reset',
    templateUrl: './password-reset.component.html',
    styleUrls: ['./styles.css']
})

export class PasswordResetComponent {
    form: FormGroup;

    constructor (fb : FormBuilder) {
        this.form = fb.group({
            oldPassword: ['', Validators.compose([Validators.required])],
            newPassword: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
            confirmNewPassword: ['', Validators.compose([Validators.required])]
        })

        this.form.setValidators(PasswordValidator.passwordsDoNotMatch);
    }

    onSubmit(e) {
        console.log("onSubmit()");
        //console.log(e);

        var oldPassword : AbstractControl = this.form.get("oldPassword");
        console.log(oldPassword);
 
        this.checkOldPasswordIsCorrect(oldPassword);
       
        if (this.form.valid)
        {
            console.log(this.form);
            alert("You've successfully changed your password!");
        }
    }

    public checkOldPasswordIsCorrect(formControl) {
        if (formControl.value == 'pass') {
            return null;
        }
        else {
            formControl.setErrors({ passwordIncorrect: true });
        }
    }


}